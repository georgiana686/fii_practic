import React, { useContext } from 'react';

import './Event.scss';
import moment from 'moment';
import { IonButton } from '@ionic/react';
import { AuthContext } from '../auth';
import api from '../../api';

const Event = ({ event, onEventChange }) => {
    const { currentUser } = useContext(AuthContext);

    const joinEvent = (event, user) => {
        api.joinEvent(event, user).then(() => {
            onEventChange();
        });
    };

    const leaveEvent = (event, user) => {
        api.leaveEvent(event, user).then(() => {
            onEventChange();
        });
    };

    const renderButton = id => {
        if (userInParticipants(id)) {
            return (
                <IonButton
                    onClick={() => leaveEvent(event.id, id)}
                    color="danger"
                    size="small"
                >
                    Leave
                </IonButton>
            );
        }

        return (
            <IonButton
                onClick={() => joinEvent(event.id, id)}
                color={
                    event.participants.length === event.max_participants
                        ? 'danger'
                        : 'success'
                }
                disabled={event.participants.length === event.max_participants}
                size="small"
            >
                Join
            </IonButton>
        );
    };

    const userInParticipants = id => {
        return (
            event.participants.filter(participant => participant.user === id)
                .length > 0
        );
    };

    return (
        <div className="event__container">
            <div className="event__details">
                <span>{event.name}</span>
                <span className="event__date">
                    {moment(event.date).format('Do MMMM YYYY, h:mm A')}
                </span>
            </div>
            <span>
                {event.participants.length}/{event.max_participants}
            </span>
            <div>{renderButton(currentUser.uid)}</div>
        </div>
    );
};

export default Event;