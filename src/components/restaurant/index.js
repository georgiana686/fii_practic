import React from 'react';
import { IonButton, IonList, getPlatforms, IonLabel } from '@ionic/react';

import './Restaurant.scss';
import Event from '../event';
import api from '../../api';
import EventAdd from '../eventAdd';

class Restaurant extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            restaurant: this.props.restaurant,
            events: []
        };
    }

    onClosePress = () => {
        this.setState({
            events: []
        });
        this.props.onClosePress();
    };

    onEventChange = () => {
        this.fetchRestaurant(this.state.restaurant.id);
    };

    fetchRestaurant = id => {
        api.fetchRestaurant(id).then(restaurantQuery => {
            if (restaurantQuery.size > 0) {
                const restaurant = restaurantQuery.docs[0];
                this.setState({
                    restaurant: {
                        ...this.state.restaurant,
                        api: {
                            ...restaurant.data(),
                            id: restaurant.id
                        }
                    }
                });
                this.fetchEventsForRestaurant(restaurant.id).then(events =>
                    this.setState({ events })
                );
            }
        });
    };

    fetchEventsForRestaurant = async id => {
        const events = [];
        let eventsQuery = await api.fetchEventsForRestaurant(id);
        if (eventsQuery.size > 0) {
            for (let i = 0; i < eventsQuery.docs.length; i++) {
                const eventQuery = eventsQuery.docs[i];
                let participantsQuery = await api.fetchParticipantsCountForEvent(
                    eventQuery.id
                );
                events.push({
                    ...eventQuery.data(),
                    id: eventQuery.id,
                    participants: participantsQuery.docs.map(particiantQuery =>
                        particiantQuery.data()
                    )
                });
            }
        }
        return events;
    };

    componentDidUpdate(prevProps) {
        if (prevProps.restaurant !== this.props.restaurant) {
            this.setState({
                restaurant: this.props.restaurant
            });
        }

        if (
            prevProps.restaurant !== this.props.restaurant &&
            this.props.restaurant !== null &&
            this.props.restaurant.api !== null
        ) {
            this.fetchRestaurant(this.props.restaurant.id);
        }
    }

    render() {
        const { restaurant, events } = this.state;
        return (
            <div
                className={`restaurant__wrapper 
                ${restaurant && 'restaurant__wrapper--opened'}`}
                data-platforms={getPlatforms()}
            >
                <div
                    className="restaurant__container"
                    data-platforms={getPlatforms()}
                >
                    <div
                        className="restaurant__image"
                        style={{
                            backgroundImage: `url(${
                                restaurant && restaurant.api
                                    ? restaurant.api.photo_url
                                    : 'https://inland-investments.com/sites/default/files/property/No-Photo-Available.jpg'
                            })`
                        }}
                    />
                    <p className="restaurant__title">
                        <span>{restaurant && restaurant.title}</span>
                        {restaurant && restaurant.api && <EventAdd restaurant={this.state.restaurant.api.id} onEventChange={this.onEventChange} />}
                    </p>
                    <p className="restaurant__description">
                        {restaurant &&
                            restaurant.api &&
                            restaurant.api.description}
                    </p>
                    {restaurant && !restaurant.api && (
                        <IonLabel>
                            Restaurant not present in database. Please register
                            it.
                        </IonLabel>
                    )}

                    <IonList className="restaurant__events">
                        {events.map((event, index) => (
                            <Event
                                key={index}
                                event={event}
                                onEventChange={this.onEventChange}
                            />
                        ))}
                    </IonList>
                    <IonButton
                        className="restaurant__close"
                        data-platforms={getPlatforms()}
                        onClick={this.onClosePress}
                    >
                        Close
                    </IonButton>
                </div>
            </div>
        );
    }
}

export default Restaurant;
