import React from 'react';
import {
    IonButton,
    IonContent,
    IonInput,
    IonItem,
    IonLabel,
    IonList,
    IonModal,
    IonDatetime
} from '@ionic/react';

import './EventAdd.scss';
import api from '../../api';
import { handleEnterKeypress } from '../../utils/handlers';

class EventAdd extends React.Component {
    constructor() {
        super();
        this.formEl = React.createRef();
        this.state = {
            showModal: false
        };
    }

    showModal = show => {
        this.setState({
            showModal: show
        });
    };

    createEvent = event => {
        event.preventDefault();
        const { name, participants, date } = event.target.elements;
        api.createEventForRestaurant({
            name: name.value,
            max_participants: +participants.value,
            date: new Date(date.value).getTime(),
            restaurant: this.props.restaurant
        }).then(() => {
            this.props.onEventChange();
            this.showModal(false);
        });
    };

    componentDidMount() {
        document.addEventListener('keydown', this.handleEnterKeypresWrapper);
    }

    componentWillUnmount() {
        document.removeEventListener('keydown', this.handleEnterKeypresWrapper);
    }

    handleEnterKeypresWrapper = ev => {
        handleEnterKeypress(ev, () => {
            ev.preventDefault();
            this.formEl.current.dispatchEvent(new Event('submit'));
        });
    };

    render() {
        const { showModal } = this.state;
        return (
            <>
                <IonModal isOpen={showModal}>
                    <IonContent>
                        <IonLabel style={{ fontSize: 20, marginBottom: 20 }}>
                            Create new event
                        </IonLabel>
                        <IonList>
                            <form
                                ref={this.formEl}
                                className="form-container"
                                onSubmit={this.createEvent}
                            >
                                <IonItem>
                                    <IonLabel position="floating">
                                        Name
                                    </IonLabel>
                                    <IonInput name="name" />
                                </IonItem>
                                <IonItem>
                                    <IonLabel position="floating">
                                        Participants
                                    </IonLabel>
                                    <IonInput
                                        type="number"
                                        name="participants"
                                    />
                                </IonItem>
                                <IonItem>
                                    <IonLabel position="floating">
                                        Date
                                    </IonLabel>
                                    <IonDatetime
                                        name="date"
                                        displayFormat="MMM DD, YYYY HH:mm"
                                    />
                                </IonItem>
                                <IonButton
                                    type="submit"
                                    style={{ width: '100%', marginTop: 20 }}
                                >
                                    Create event
                                </IonButton>
                            </form>
                        </IonList>
                    </IonContent>
                </IonModal>
                <IonButton size="small" onClick={() => this.showModal(true)}>
                    Add
                </IonButton>
            </>
        );
    }
}

export default EventAdd;
