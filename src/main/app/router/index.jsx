import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import { ROUTE_DEFAULT, ROUTE_HOME, ROUTE_LOGIN, ROUTE_REGISTRATION } from '../../../utils/routes';

import Home from '../../pages/home';
import { Redirect } from 'react-router';
import PrivateRoute from '../../../components/privateRoute';
import Registration from '../../pages/registration';
import Login from '../../pages/login';

const Router = () => {
    return (
        <BrowserRouter>
            <Switch>
                <Route exact path={ROUTE_LOGIN} component={Login} />
                <Route
                    exact
                    path={ROUTE_REGISTRATION}
                    component={Registration}
                />
                <PrivateRoute name="home" path={ROUTE_HOME} component={Home} />
                <Route
                    path={ROUTE_DEFAULT}
                    exact
                    render={() => <Redirect to={ROUTE_HOME} />}
                />
            </Switch>
        </BrowserRouter>
    );
};

export default Router;
